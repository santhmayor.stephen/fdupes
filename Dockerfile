FROM debian:stable-slim
RUN apt-get update && apt-get install -y \
    build-essential \
    pkg-config \
	autoconf \
    automake \
    make \
    libncurses5-dev \
    libncursesw5-dev \
    libz-dev \
    libssl-dev \
    libpcre2-dev \
    libevent-dev

RUN mkdir -p /usr/src/fdupes
WORKDIR /usr/src/fdupes
COPY . .

RUN autoreconf --install && \
   ./configure && \
   make
RUN mkdir /BUILT_ASSETS && \
    cp ./fdupes /BUILT_ASSETS
